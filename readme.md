### Services
1. Mariadb
2. Wordpress FPM
3. Nginx

### docker-compose.yml
```
version: '3.7'
services:
  db:
    image: mariadb:latest
    #  volumes:
    #    - db_data:/var/lib/mysql
    restart: always
    environment:
      MYSQL_ROOT_PASSWORD: transybao
      MYSQL_DATABASE: wordpress
      MYSQL_USER: transybao
      MYSQL_PASSWORD: transybao

  wordpress:
    depends_on:
      - db
    image: wordpress:latest
    restart: always
    environment:
      WORDPRESS_DB_HOST: db:3306
      WORDPRESS_DB_USER: transybao
      WORDPRESS_DB_PASSWORD: transybao
      WORDPRESS_DB_NAME: wordpress

  phpmyadmin:
    image: phpmyadmin/phpmyadmin:latest
    ports:
      - 8181:80
    environment:
      PMA_HOST: db
      PMA_PORT: 3306
    links:
      - db

  nginx:
    container_name: nginx_reverse_proxy
    build:
      context: .
      dockerfile: nginx.dockerfile
    volumes:
      - ./nginx-conf/nginx.conf:/etc/nginx/nginx.conf:ro
    ports:
      - 80:80
    depends_on:
      - wordpress
    restart: on-failure
    
volumes:
  nginx-conf:
  # db_data:
```
